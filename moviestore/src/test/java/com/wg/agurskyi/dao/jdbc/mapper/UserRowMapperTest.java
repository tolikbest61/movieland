package com.wg.agurskyi.dao.jdbc.mapper;

import com.wg.agurskyi.entity.User;
import org.junit.Test;
import java.sql.ResultSet;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserRowMapperTest {

    @Test
    public void testMapRow() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.getInt(any())).thenReturn(2);
        when(resultSet.getString(any())).thenReturn("Дарлин").thenReturn("Эдвардс").thenReturn("darlene.edwards15@example.com").thenReturn("bricks");
        UserRowMapper userRowMapper = new UserRowMapper();
        User user = userRowMapper.mapRow(resultSet, 0);
        assertEquals(user.getId(), 2);
        assertEquals(user.getFirstName(), "Дарлин");
        assertEquals(user.getLastName(), "Эдвардс");
        assertEquals(user.getLogin(), "darlene.edwards15@example.com");
        assertEquals(user.getPassword(), "bricks");
    }
}