package com.wg.agurskyi.dao.jdbc.mapper;

import com.wg.agurskyi.entity.Genre;
import org.junit.Test;
import java.sql.ResultSet;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GenreRowMapperTest {

    @Test
    public void testMapRow() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.getInt(any())).thenReturn(7);
        when(resultSet.getString(any())).thenReturn("комедия");
        GenreRowMapper genreRowMapper = new GenreRowMapper();
        Genre genre = genreRowMapper.mapRow(resultSet, 0);
        assertEquals(genre.getId(), 7);
        assertEquals(genre.getName(), "комедия");
    }
}