package com.wg.agurskyi.dao.jdbc.mapper;

import com.wg.agurskyi.entity.Country;
import org.junit.Test;
import java.sql.ResultSet;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CountryRowMapperTest {

    @Test
    public void testMapRow() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.getInt(any())).thenReturn(1);
        when(resultSet.getString(any())).thenReturn("France");
        CountryRowMapper countryRowMapper = new CountryRowMapper();
        Country country = countryRowMapper.mapRow(resultSet, 0);
        assertEquals(country.getId(), 1);
        assertEquals(country.getName(), "France");
    }
}