package com.wg.agurskyi.dao.jdbc.mapper;

import com.wg.agurskyi.entity.Movie;
import org.junit.Test;
import java.sql.ResultSet;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MovieRowMapperTest {

    @Test
    public void testMapRow() throws Exception {
        // prepare
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.getInt(any())).thenReturn(1).thenReturn(2001);
        when(resultSet.getString(any())).thenReturn("Побег из Шоушенка").thenReturn("The Shawshank Redemption").thenReturn("France").thenReturn("4,17").thenReturn("3,12,17");
        when(resultSet.getDouble(any())).thenReturn(7.2).thenReturn(101.2);
        MovieRowMapper movieRowMapper = new MovieRowMapper();
        // when
        Movie movie = movieRowMapper.mapRow(resultSet, 0);
        // then
        assertEquals(movie.getId(), 1);
        assertEquals(movie.getMovieNameRu(), "Побег из Шоушенка");
        assertEquals(movie.getMovieNameEn(), "The Shawshank Redemption");
        assertEquals(movie.getYear(), 2001);
        assertEquals(movie.getDescription(), "France");
        assertEquals(movie.getRating(), 7.2d, 0.00);
        assertEquals(movie.getPrice(), 101.2d, 0.00001);
        assertEquals(movie.getGenres().get(1).getId(),17);
        assertEquals(movie.getCountries().get(0).getId(),3);
    }
}