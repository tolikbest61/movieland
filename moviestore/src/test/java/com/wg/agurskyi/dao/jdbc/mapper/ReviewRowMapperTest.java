package com.wg.agurskyi.dao.jdbc.mapper;

import com.wg.agurskyi.entity.Review;
import com.wg.agurskyi.entity.User;
import org.junit.Test;

import java.sql.ResultSet;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReviewRowMapperTest {

    @Test
    public void testMapRow() throws Exception {

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.getInt(any())).thenReturn(2).thenReturn(8);
        when(resultSet.getString(any())).thenReturn("Дарлин");
        ReviewRowMapper reviewRowMapper = new ReviewRowMapper();
        Review review = reviewRowMapper.mapRow(resultSet, 0);
        assertEquals(review.getId(), 2);
        assertEquals(review.getMovie().getId(), 8);
        assertEquals(review.getUser().getId(), 8);
        assertEquals(review.getComment(), "Дарлин");
    }
}