package com.wg.agurskyi.util;

import com.wg.agurskyi.entity.Movie;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class MovieComparatorTest {

    static final String ASC = "ASC";
    static final String DESC = "DESC";
    static final String EMPTY = null;
    static final String INCORRECT = "abc";

    static Movie movie1 = new Movie();
    static Movie movie2 = new Movie();
    static Movie movie3 = new Movie();
    static Movie movie4 = new Movie();
    static List<Movie> movies;

    @BeforeClass
    public static void prepareBeforeClass() throws Exception {

        movie1.setId(1);
        movie1.setMovieNameRu("Побег из Шоушенка");
        movie1.setMovieNameEn("The Shawshank Redemption");
        movie1.setYear(1994);
        movie1.setDescription("Класний фильм");
        movie1.setRating(7.0);
        movie1.setPrice(199.0);

        movie2.setId(2);
        movie2.setMovieNameRu("Блеф");
        movie2.setMovieNameEn("Bluff storia di truffe e di imbroglioni");
        movie2.setYear(1976);
        movie2.setDescription("Класний фильм");
        movie2.setRating(7.6);
        movie2.setPrice(199.0);

        movie3.setId(3);
        movie3.setMovieNameRu("Зеленая миля");
        movie3.setMovieNameEn("The Green Mile");
        movie3.setYear(1999);
        movie3.setDescription("Класний фильм");
        movie3.setRating(7.0);
        movie3.setPrice(115.0);

        movie4.setId(4);
        movie4.setMovieNameRu("Форрест Гамп");
        movie4.setMovieNameEn("Forrest Gump");
        movie4.setYear(1994);
        movie4.setDescription("Класний фильм");
        movie4.setRating(8.6);
        movie4.setPrice(57.0);
    }

    @Before
    public void prepareBefore(){
        movies = new ArrayList<>();
        movies.add(movie1);
        movies.add(movie2);
        movies.add(movie3);
        movies.add(movie4);
    }

    @Test
    public void testSortRatingAscPriceAsc(){
        Collections.sort(movies,new MovieComparator(ASC,ASC));
        assertEquals(movies.get(0),movie3);
        assertEquals(movies.get(1),movie1);
        assertEquals(movies.get(2),movie2);
        assertEquals(movies.get(3),movie4);
    }

    @Test
    public void testSortRatingAscPriceDesc(){
        Collections.sort(movies,new MovieComparator(ASC,DESC));
        assertEquals(movies.get(0),movie1);
        assertEquals(movies.get(1),movie3);
        assertEquals(movies.get(2),movie2);
        assertEquals(movies.get(3),movie4);
    }

    @Test
    public void testSortRatingDescPriceAsc(){
        Collections.sort(movies,new MovieComparator(DESC,ASC));
        assertEquals(movies.get(0),movie4);
        assertEquals(movies.get(1),movie2);
        assertEquals(movies.get(2),movie3);
        assertEquals(movies.get(3),movie1);
    }

    @Test
    public void testSortRatingDescPriceDesc(){
        Collections.sort(movies,new MovieComparator(DESC,DESC));
        assertEquals(movies.get(0),movie4);
        assertEquals(movies.get(1),movie2);
        assertEquals(movies.get(2),movie1);
        assertEquals(movies.get(3),movie3);
    }

    @Test
    public void testSortRatingAsc(){
        Collections.sort(movies,new MovieComparator(ASC,EMPTY));
        assertEquals(movies.get(0),movie1);
        assertEquals(movies.get(1),movie3);
        assertEquals(movies.get(2),movie2);
        assertEquals(movies.get(3),movie4);
    }

    @Test
    public void testSortRatingDesc(){
        Collections.sort(movies,new MovieComparator(DESC,EMPTY));
        assertEquals(movies.get(0),movie4);
        assertEquals(movies.get(1),movie2);
        assertEquals(movies.get(2),movie1);
        assertEquals(movies.get(3),movie3);
    }

    @Test
    public void testSortPriceAsc(){
        Collections.sort(movies,new MovieComparator(EMPTY,ASC));
        assertEquals(movies.get(0),movie4);
        assertEquals(movies.get(1),movie3);
        assertEquals(movies.get(2),movie1);
        assertEquals(movies.get(3),movie2);
    }

    @Test
    public void testSortPriceDesc(){
        Collections.sort(movies,new MovieComparator(EMPTY,DESC));
        assertEquals(movies.get(0),movie1);
        assertEquals(movies.get(1),movie2);
        assertEquals(movies.get(2),movie3);
        assertEquals(movies.get(3),movie4);
    }

    @Test
    public void testSortRatingIncorrectPriceIncorrect(){
        Collections.sort(movies,new MovieComparator(INCORRECT,INCORRECT));
        assertEquals(movies.get(0),movie1);
        assertEquals(movies.get(1),movie2);
        assertEquals(movies.get(2),movie3);
        assertEquals(movies.get(3),movie4);
    }
}