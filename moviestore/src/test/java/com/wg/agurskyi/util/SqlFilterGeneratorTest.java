package com.wg.agurskyi.util;

import com.wg.agurskyi.request.MovieSearchRequest;
import org.junit.Test;
import static org.junit.Assert.*;

public class SqlFilterGeneratorTest {

    @Test
    public void testBuildSqlFilter() throws Exception {
        MovieSearchRequest movieSearchRequest = new MovieSearchRequest();
        movieSearchRequest.setName("Inception");
        movieSearchRequest.setYear(2010);
        movieSearchRequest.setCountry("США");
        movieSearchRequest.setGenre("детектив");
        String sqlFilter = SqlGenerator.buildSqlFilter("",movieSearchRequest);
        assertTrue(sqlFilter.contains("year = 2010"));
        assertTrue(sqlFilter.contains("movie_name_en like '%Inception%'"));
        assertTrue(sqlFilter.contains("country like '%США%'"));
        assertTrue(sqlFilter.contains("genre like '%детектив%'"));
    }
}