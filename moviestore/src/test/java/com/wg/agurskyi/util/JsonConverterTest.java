package com.wg.agurskyi.util;

import com.wg.agurskyi.request.MovieSearchRequest;
import org.junit.Test;

import static org.junit.Assert.*;

public class JsonConverterTest {

    @Test
    public void testParseSearchRequest() throws Exception {
        JsonConverter jsonConverter = new JsonConverter();
        String json = "{\"year\": \"2011\",\"genre\": \"драма\",\"country\": \"Україна\",\"name\": \"Тарас Бульба\"}";
        MovieSearchRequest movieSearchRequest = jsonConverter.parseSearchRequest(json);
        assertEquals(movieSearchRequest.getYear(),2011);
        assertEquals(movieSearchRequest.getGenre(),"драма");
        assertEquals(movieSearchRequest.getCountry(),"Україна");
        assertEquals(movieSearchRequest.getName(),"Тарас Бульба");
    }
}