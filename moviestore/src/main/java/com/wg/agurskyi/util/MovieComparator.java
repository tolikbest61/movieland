package com.wg.agurskyi.util;

import com.wg.agurskyi.entity.Movie;
import java.util.Comparator;
import com.wg.agurskyi.util.enums.SortingOrder;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MovieComparator implements Comparator<Movie> {

    private SortingOrder ratingOrder;
    private SortingOrder priceOrder;
    private final Logger log = LoggerFactory.getLogger(getClass());

    public MovieComparator(String ratingOrder, String priceOrder) {
        if (ratingOrder != null) {
            try {
                this.ratingOrder = SortingOrder.valueOf(ratingOrder.toUpperCase());
            } catch (Exception ex) {
                log.error("Incorrect sort type for rating: {}", ex);
            }
        }
        if (priceOrder != null) {
            try {
                this.priceOrder = SortingOrder.valueOf(priceOrder.toUpperCase());
            } catch (Exception ex) {
                log.error("Incorrect sort type for price: {}", ex);
            }
        }
    }

    @Override
    public int compare(Movie movie1, Movie movie2) {
        CompareToBuilder compareToBuilder = new CompareToBuilder();
        if (ratingOrder == SortingOrder.ASC) {
            compareToBuilder.append(movie1.getRating(), movie2.getRating());
        } else if (ratingOrder == SortingOrder.DESC) {
            compareToBuilder.append(movie2.getRating(), movie1.getRating());
        }
        if (priceOrder == SortingOrder.ASC) {
            compareToBuilder.append(movie1.getPrice(), movie2.getPrice());
        } else if (priceOrder == SortingOrder.DESC) {
            compareToBuilder.append(movie2.getPrice(), movie1.getPrice());
        }
        return compareToBuilder.toComparison();
    }
}