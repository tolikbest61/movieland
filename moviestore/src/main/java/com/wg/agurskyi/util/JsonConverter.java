package com.wg.agurskyi.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wg.agurskyi.entity.Movie;
import com.wg.agurskyi.request.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class JsonConverter {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private ObjectMapper objectMapper = new ObjectMapper();

    public String toJson(Object obj) {
        String json = "";
        log.info("Start transform object {} to json ", obj);
        long startTime = System.currentTimeMillis();
        try {
            json = objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException ex) {
            log.error("Cannot convert object to json. Error: {}", ex);
        }
        long time = System.currentTimeMillis() - startTime;
        log.info("Json {} is received. It took {} ms", json, time);
        return json;
    }

    public MovieSearchRequest parseSearchRequest(String json){
        MovieSearchRequest movieSearchRequest = null;
        try {
            movieSearchRequest = objectMapper.readValue(json, MovieSearchRequest.class);
        }
        catch (Exception ex) {
            log.error("Cannot convert json {} to MovieSearchRequest. Error: {}", json, ex);
        }
        return movieSearchRequest;
    }

    public UserCredentialsRequest parseUserCredentialsRequest(String json){
        UserCredentialsRequest userCredentialsRequest = null;
        try {
            userCredentialsRequest = objectMapper.readValue(json, UserCredentialsRequest.class);
        }
        catch (Exception ex) {
            log.error("Cannot convert json {} to UserCredentialsRequest. Error: {}", json, ex);
        }
        return userCredentialsRequest;
    }

    public MovieRequest parseMovieRequest(String json){
        try {
            return  objectMapper.readValue(json, MovieRequest.class);
        }
        catch (Exception ex) {
            log.error("Cannot convert json {} to MovieRequest. Error: {}", json, ex);
            throw new RuntimeException("Cannot convert json to MovieRequest");
        }
    }

    public ReviewRequest parseReviewRequest(String json){
        try {
            return objectMapper.readValue(json, ReviewRequest.class);
        }
        catch (Exception ex) {
            log.error("Cannot convert json {} to ReviewRequest. Error: {}", json, ex);
            throw new RuntimeException("Cannot convert json to ReviewRequest");
        }
    }

    public RatingRequest parseRatingRequest(String json){
        try {
            return objectMapper.readValue(json, RatingRequest.class);
        }
        catch (Exception ex) {
            log.error("Cannot convert json {} to RatingRequest. Error: {}", json, ex);
            throw new RuntimeException("Cannot convert json to RatingRequest");
        }
    }
}
