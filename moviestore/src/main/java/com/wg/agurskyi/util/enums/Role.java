package com.wg.agurskyi.util.enums;

public enum Role {

    ADMIN(0), USER(1), GUEST(2);

    private int id;

    Role(int id) {
        this.id = id;
    }

    public static Role getById(int id) {
        for (Role role : Role.values()) {
            if (role.id == id) {
                return role;
            }
        }
        return GUEST;
    }
}
