package com.wg.agurskyi.util;

import com.wg.agurskyi.dto.DetailMovieDto;
import com.wg.agurskyi.dto.MovieDto;
import com.wg.agurskyi.entity.Movie;
import com.wg.agurskyi.entity.Review;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DaoToDtoConvertor {

    public static MovieDto convertToMovieDto(Movie movie) {
        MovieDto movieDto = new MovieDto();
        movieDto.setMovieNameRu(movie.getMovieNameRu());
        movieDto.setMovieNameOriginal(movie.getMovieNameEn());
        movieDto.setYear(movie.getYear());
        movieDto.setRating(movie.getRating());
        movieDto.setGenres(movie.getGenres());
        return movieDto;
    }

    public static DetailMovieDto convertToDetailMovieDto(Movie movie, List<Review> reviewList) {

        Random random = new Random();
        List<Review> randomReview = new ArrayList<>();

        DetailMovieDto detailMovieDto = new DetailMovieDto();
        detailMovieDto.setMovieNameRu(movie.getMovieNameRu());
        detailMovieDto.setMovieNameOriginal(movie.getMovieNameEn());
        detailMovieDto.setYear(movie.getYear());
        detailMovieDto.setRating(movie.getRating());
        detailMovieDto.setGenre(movie.getGenres());
        detailMovieDto.setCountry(movie.getCountries());
        if (reviewList.size() > 2) {
            for (int j = 0; j < 2; j++) {
                int index = random.nextInt(reviewList.size());
                randomReview.add(reviewList.get(index));
                reviewList.remove(index);
            }
            reviewList = randomReview;
        }
        detailMovieDto.setReview(reviewList);
        detailMovieDto.setDescription(movie.getDescription());
        return detailMovieDto;
    }
}