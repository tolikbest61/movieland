package com.wg.agurskyi.util.enums;

public enum SortingOrder {

    ASC("ASC"), DESC("DESC");

    private String name;

    SortingOrder(String name) {
        this.name = name;
    }
}
