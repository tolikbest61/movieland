package com.wg.agurskyi.util;

import com.wg.agurskyi.request.MovieSearchRequest;
import com.wg.agurskyi.util.enums.SortingOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlGenerator {

    private static final Logger log = LoggerFactory.getLogger("SqlGenerator");

    public static String buildSqlFilter(String sqlQuery, MovieSearchRequest movieSearchRequest) {
        StringBuilder resultSqlQuery = new StringBuilder(sqlQuery.substring(0, sqlQuery.length() - 1));
        if (movieSearchRequest.getGenre() != null) {
            resultSqlQuery.append(" and genre like '%" + movieSearchRequest.getGenre() + "%'");
        }
        if (movieSearchRequest.getName() != null) {
            resultSqlQuery.append(" and movie_name_en like '%" + movieSearchRequest.getName() + "%'");
        }
        if (movieSearchRequest.getYear() != 0) {
            resultSqlQuery.append(" and year = " + movieSearchRequest.getYear());
        }
        if (movieSearchRequest.getCountry() != null) {
            resultSqlQuery.append(" and country like '%" + movieSearchRequest.getCountry() + "%'");
        }
        resultSqlQuery.append(";");
        return resultSqlQuery.toString();
    }

    public static String buildSortingSql(String sqlQuery, String ratingOrder, String priceOrder, int page) {
        StringBuilder resultSqlQuery = new StringBuilder(sqlQuery.substring(0, sqlQuery.length() - 1));
        if (ratingOrder != null || priceOrder != null) {
            resultSqlQuery.append(" order by");
            if (ratingOrder != null) {
                try {
                    resultSqlQuery.append(" rating " + SortingOrder.valueOf(ratingOrder.toUpperCase())
                            + (priceOrder != null ? "," : ""));
                } catch (Exception ex) {
                    log.error("Incorrect sort type for rating: {}", ex);
                }
            }
            if (priceOrder != null) {
                try {
                    resultSqlQuery.append(" price " + SortingOrder.valueOf(priceOrder.toUpperCase()));
                } catch (Exception ex) {
                    log.error("Incorrect sort type for price: {}", ex);
                }
            }
        }
        resultSqlQuery.append(" offset " + (Constant.MOVIES_PER_PAGE * (page-1)) + " limit " +
                Constant.MOVIES_PER_PAGE).append(";");
        return resultSqlQuery.toString();
    }
}
