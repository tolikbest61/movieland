package com.wg.agurskyi.util;

public class Constant {

    public static final String COMMA_DELIMITER = ",";
    public static final String TOKEN = "token";
    public static final String AUTHORIZED_USER = "authorizedUser";
    public static final int MOVIES_PER_PAGE = 5;
}
