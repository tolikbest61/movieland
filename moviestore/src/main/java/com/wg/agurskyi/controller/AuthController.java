package com.wg.agurskyi.controller;

import com.wg.agurskyi.service.UserService;
import com.wg.agurskyi.util.Constant;
import com.wg.agurskyi.util.JsonConverter;
import com.wg.agurskyi.request.UserCredentialsRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RequestMapping(value="/v1/login")
@Controller
public class AuthController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;

    @Autowired
    private JsonConverter jsonConverter;

    @RequestMapping(method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> getToken(@RequestBody String credentialsRequest, HttpServletResponse response) {
        log.info("Get user token for credentials request");
        long startTime = System.currentTimeMillis();
        UserCredentialsRequest userCredentialsRequest = jsonConverter.parseUserCredentialsRequest(credentialsRequest);
        if (null != userCredentialsRequest) {
            try {
                String token = userService.authoriseUser(userCredentialsRequest).getToken();
                log.info("Token {} was got. It took {} ms", token, System.currentTimeMillis() - startTime);
                Cookie cookie = new Cookie(Constant.TOKEN, token);
                cookie.setMaxAge(2*60*60);
                response.addCookie(cookie);
                return new ResponseEntity<>(token, HttpStatus.OK);
            } catch (Exception ex) {
                log.error("Authorization is failed: {}", ex);
                return new ResponseEntity<>(jsonConverter.toJson(ex), HttpStatus.BAD_REQUEST);
            }
        }
        log.info("Failed user credential request");
        return new ResponseEntity<>("Failed user credential request",HttpStatus.BAD_REQUEST);
    }
}
