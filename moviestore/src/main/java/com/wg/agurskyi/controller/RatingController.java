package com.wg.agurskyi.controller;

import com.wg.agurskyi.annotation.AuthRequired;
import com.wg.agurskyi.entity.Rating;
import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.request.RatingRequest;
import com.wg.agurskyi.service.MovieService;
import com.wg.agurskyi.service.RatingService;
import com.wg.agurskyi.util.Constant;
import com.wg.agurskyi.util.JsonConverter;
import com.wg.agurskyi.util.enums.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@RequestMapping(value = "/v1/rate")
@Controller
public class RatingController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JsonConverter jsonConverter;

    @Autowired
    private RatingService ratingService;

    @Autowired
    private MovieService movieService;

    @AuthRequired(role = Role.USER)
    @RequestMapping(method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> add(@RequestBody String addRatingRequest, HttpServletRequest request) {
        log.info("Add rating {} into DB", addRatingRequest);
        long startTime = System.currentTimeMillis();
        try {
            User user = (User) request.getAttribute(Constant.AUTHORIZED_USER);
            if (user == null) {
                String message = "Cannot define user. Authorisation error";
                log.warn(message);
                return new ResponseEntity<>(message, HttpStatus.UNAUTHORIZED);
            }
            RatingRequest ratingRequest = jsonConverter.parseRatingRequest(addRatingRequest);
            if (ratingRequest.getRating() < 1 || ratingRequest.getRating() > 10) {
                String message = "Rating must be between 1 and 10";
                log.warn(message);
                return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            }
            if (movieService.getById(ratingRequest.getMovieId()) == null) {
                String message = "Movie with id=" + ratingRequest.getMovieId() + " is absent in DB";
                log.warn(message);
                return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            }
            Rating rating = createRating(ratingRequest, user);
            ratingService.addOrModify(rating);
            log.info("Rating {} was added. It took {} ms", rating.toString(), System.currentTimeMillis() - startTime);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            log.warn("Rating was not added. Error: {}", ex);
            return new ResponseEntity<>(jsonConverter.toJson(ex), HttpStatus.BAD_REQUEST);
        }
    }

    private Rating createRating(RatingRequest ratingRequest, User user) {
        Rating rating = new Rating();
        rating.setMovieId(ratingRequest.getMovieId());
        rating.setAuthorId(user.getId());
        rating.setRating(ratingRequest.getRating());
        return rating;
    }
}
