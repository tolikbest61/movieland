package com.wg.agurskyi.controller;

import com.wg.agurskyi.annotation.AuthRequired;
import com.wg.agurskyi.dto.MovieDto;
import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.request.MovieRequest;
import com.wg.agurskyi.request.MovieSearchRequest;
import com.wg.agurskyi.entity.Movie;
import com.wg.agurskyi.entity.Review;
import com.wg.agurskyi.service.MovieService;
import com.wg.agurskyi.service.ReviewService;
import com.wg.agurskyi.util.Constant;
import com.wg.agurskyi.util.DaoToDtoConvertor;
import com.wg.agurskyi.util.JsonConverter;
import com.wg.agurskyi.util.enums.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping(value="/v1/movie")
@Controller
public class MovieController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private MovieService movieService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private JsonConverter jsonConverter;

    @RequestMapping(produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> getAll(@RequestParam (value = "rating", required = false) String ratingOrder,
                                         @RequestParam (value = "price", required = false) String priceOrder,
                                         @RequestParam (value = "page", defaultValue = "1") Integer page) {
        log.info("Sending request to get all movies");
        long startTime = System.currentTimeMillis();
        List<Movie> movies = movieService.getAll(ratingOrder,priceOrder,page);
        List<MovieDto> movieDtos = movies.stream().map(DaoToDtoConvertor::convertToMovieDto).collect(Collectors.toList());
        String movieJson = jsonConverter.toJson(movieDtos);
        log.info("Movies {} were received. It took {} ms", movieJson, System.currentTimeMillis() - startTime);
        return new ResponseEntity<>(movieJson, HttpStatus.OK);
    }

    @AuthRequired(role = Role.USER)
    @RequestMapping(value="/{movieId}", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> getMovieById(@PathVariable int movieId) {
        log.info("Sending request to get movie with id = {}", movieId);
        long startTime = System.currentTimeMillis();
        Movie movie = movieService.getById(movieId);
        List<Review> reviewList = reviewService.getByMovieId(movieId);
        String movieJson = jsonConverter.toJson(DaoToDtoConvertor.convertToDetailMovieDto(movie,reviewList));
        log.info("Movie {} is received. It took {} ms", movie, System.currentTimeMillis() - startTime);
        return new ResponseEntity<>(movieJson, HttpStatus.OK);
    }

    @RequestMapping(value="/search", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> search(@RequestBody String searchRequest) {
        log.info("Searching movie by request to get movies with next input values: {}", searchRequest);
        long startTime = System.currentTimeMillis();
        String movieJson = null;
        MovieSearchRequest movieSearchRequest = jsonConverter.parseSearchRequest(searchRequest);
        if (movieSearchRequest != null) {
            List<Movie> movies = movieService.search(movieSearchRequest);
            if (movies != null) {
                List<MovieDto> movieDtos = movies.stream().map(DaoToDtoConvertor::convertToMovieDto).collect(Collectors.toList());
                movieJson = jsonConverter.toJson(movieDtos);
                log.info("Movies {} are searched. It took {} ms", movieJson.toString(), System.currentTimeMillis() - startTime);
                return new ResponseEntity<>(movieJson, HttpStatus.OK);
            }
        }
        String message = "Moview " + movieJson + " not found in DB";
        log.warn(message);
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @AuthRequired(role = Role.ADMIN)
    @RequestMapping(method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> add(@RequestBody String addMovieRequest, HttpServletRequest request) {
        log.info("Add new movie: {}", addMovieRequest);
        long startTime = System.currentTimeMillis();
        try {
            User user = (User) request.getAttribute(Constant.AUTHORIZED_USER);
            if (user == null) {
                String message = "Cannot define user. Authorisation error";
                log.warn(message);
                return new ResponseEntity<>(message, HttpStatus.UNAUTHORIZED);
            }
            MovieRequest movieRequest = jsonConverter.parseMovieRequest(addMovieRequest);
            Movie movie = movieService.add(movieRequest);
            log.info("Movie {} was added. It took {} ms", movie.toString(), System.currentTimeMillis() - startTime);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            log.error("Movie was not added. Error: {}", ex);
            return new ResponseEntity<>(jsonConverter.toJson(ex), HttpStatus.BAD_REQUEST);
        }
    }

    @AuthRequired(role = Role.ADMIN)
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> edit(@RequestBody String editMovieRequest, HttpServletRequest request) {
        log.info("Add new movie: {}", editMovieRequest);
        long startTime = System.currentTimeMillis();
        try {
            User user = (User) request.getAttribute(Constant.AUTHORIZED_USER);
            if (user == null) {
                String message = "Cannot define user. Authorisation error";
                log.warn(message);
                return new ResponseEntity<>(message, HttpStatus.UNAUTHORIZED);
            }
            MovieRequest movieRequest = jsonConverter.parseMovieRequest(editMovieRequest);
            Movie movie = movieService.edit(movieRequest);
            log.info("Movie {} was added. It took {} ms", movie.toString(), System.currentTimeMillis() - startTime);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            log.warn("Movie was not added. Error: {}", ex);
            return new ResponseEntity<>(jsonConverter.toJson(ex), HttpStatus.BAD_REQUEST);
        }
    }

    @AuthRequired(role = Role.ADMIN)
    @RequestMapping(value = "/{movieId}", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> markForDelete(@PathVariable int movieId, HttpServletRequest request) {
        log.info("Mark movie with id={} for delete", movieId);
        long startTime = System.currentTimeMillis();
        try {
            User user = (User) request.getAttribute(Constant.AUTHORIZED_USER);
            if (user == null) {
                String message = "Cannot define user. Authorisation error";
                log.warn(message);
                return new ResponseEntity<>(message, HttpStatus.UNAUTHORIZED);
            }
            Movie movie = movieService.markForDelete(movieId);
            log.info("Movie {} was marked for delete. It took {} ms", movie.toString(), System.currentTimeMillis() - startTime);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            log.warn("Movie was not marked. Error: {}", ex);
            return new ResponseEntity<>(jsonConverter.toJson(ex), HttpStatus.BAD_REQUEST);
        }
    }

    @AuthRequired(role = Role.ADMIN)
    @RequestMapping(value = "/{movieId}/unmark", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> unmarkForDelete(@PathVariable int movieId, HttpServletRequest request) {
        log.info("Unmark movie with id={} for delete", movieId);
        long startTime = System.currentTimeMillis();
        try {
            User user = (User) request.getAttribute(Constant.AUTHORIZED_USER);
            if (user == null) {
                String message = "Cannot define user. Authorisation error";
                log.warn(message);
                return new ResponseEntity<>(message, HttpStatus.UNAUTHORIZED);
            }
            Movie movie = movieService.unmarkForDelete(movieId);
            log.info("Movie {} was unmarked. It took {} ms", movie, System.currentTimeMillis() - startTime);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            log.warn("Movie was not unmarked. Error: {}", ex);
            return new ResponseEntity<>(jsonConverter.toJson(ex), HttpStatus.BAD_REQUEST);
        }
    }
}
