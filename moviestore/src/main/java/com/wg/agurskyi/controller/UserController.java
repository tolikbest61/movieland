package com.wg.agurskyi.controller;

import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.service.UserService;
import com.wg.agurskyi.util.JsonConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/v1/user/")
public class UserController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;

    @Autowired
    private JsonConverter jsonConverter;

    @RequestMapping(value="/{userId}", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> getMovieById(@PathVariable int userId) {
        log.info("Sending request to get user with id = {}", userId);
        long startTime = System.currentTimeMillis();
        User user = userService.getById(userId);
        log.info("User {} is received. It took {} ms", user, System.currentTimeMillis() - startTime);
        return new ResponseEntity<>(jsonConverter.toJson(user), HttpStatus.OK);
    }
}
