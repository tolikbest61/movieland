package com.wg.agurskyi.controller;

import com.wg.agurskyi.annotation.AuthRequired;
import com.wg.agurskyi.entity.Review;
import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.request.ReviewRequest;
import com.wg.agurskyi.service.ReviewService;
import com.wg.agurskyi.util.Constant;
import com.wg.agurskyi.util.JsonConverter;
import com.wg.agurskyi.util.enums.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping(value = "/v1/review")
@Controller
public class ReviewController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private JsonConverter jsonConverter;

    @AuthRequired(role = Role.USER)
    @RequestMapping(method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> addReview(@RequestBody String addReviewRequest, HttpServletRequest request) {
        log.info("Add review {} into DB", addReviewRequest);
        long startTime = System.currentTimeMillis();
        try {
            ReviewRequest reviewRequest = jsonConverter.parseReviewRequest(addReviewRequest);
            User user = (User) request.getAttribute(Constant.AUTHORIZED_USER);
            if (user.getId() != reviewRequest.getAuthorId()) {
                String message = "You can only add review for your own user";
                log.warn(message);
                return new ResponseEntity<>(message, HttpStatus.UNAUTHORIZED);
            }
            Review review = reviewService.addReview(reviewRequest);
            String reviewJson = jsonConverter.toJson(review);
            log.info("Review {} was added. It took {} ms", reviewJson.toString(), System.currentTimeMillis() - startTime);
            return new ResponseEntity<>(reviewJson, HttpStatus.OK);
        } catch (Exception ex) {
            log.warn("Review was not added. Error: {}", ex);
            return new ResponseEntity<>(jsonConverter.toJson(ex), HttpStatus.BAD_REQUEST);
        }
    }

    @AuthRequired(role = Role.USER)
    @RequestMapping(value = "/{reviewId}", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<String> remove(@PathVariable int reviewId, HttpServletRequest request) {
        log.info("Remove review with id={} from DB", reviewId);
        long startTime = System.currentTimeMillis();
        try {
            User user = (User) request.getAttribute(Constant.AUTHORIZED_USER);
            Review review = reviewService.getById(reviewId);
            if (review == null) {
                String message = "Review with id=" + reviewId + " not found in DB";
                log.warn(message);
                return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            }
            if (user.getId() != review.getUser().getId()) {
                String message = "You can only remove review for your own user";
                log.warn(message);
                return new ResponseEntity<>(message, HttpStatus.UNAUTHORIZED);
            }
            reviewService.removeReview(reviewId);
            log.info("Review {} was removed. It took {} ms", review, System.currentTimeMillis() - startTime);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            log.warn("Review was not removed. Error: {}", ex);
            return new ResponseEntity<>(jsonConverter.toJson(ex), HttpStatus.BAD_REQUEST);
        }
    }
}
