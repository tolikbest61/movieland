package com.wg.agurskyi.interceptor;

import com.wg.agurskyi.annotation.AuthRequired;
import com.wg.agurskyi.entity.UserToken;
import com.wg.agurskyi.service.SecurityService;
import com.wg.agurskyi.util.Constant;
import com.wg.agurskyi.util.enums.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthInterceptor extends HandlerInterceptorAdapter{

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private SecurityService securityService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        AuthRequired authRequired = handlerMethod.getMethod().getAnnotation(AuthRequired.class);
        if (authRequired == null) {
            return true;
        }
        String token = null;
        if (request.getCookies() != null) {
            Cookie[] cookie = request.getCookies();
            for (int i = 0; i < cookie.length; i++) {
                if (cookie[i].getName().equals(Constant.TOKEN)) {
                    token = cookie[i].getValue();
                }
            }
        }
        if (token == null) {
            String message = "Please authorize. Token not found";
            log.error(message);
            throw new SecurityException(message);
        }
        try {
            UserToken userToken = securityService.validateToken(token);
            Role authRole = authRequired.role();
            if (!(Role.getById(userToken.getUser().getRole()) == authRole)) {
                throw new SecurityException("Role required: " + authRole);
            }
            request.setAttribute(Constant.AUTHORIZED_USER,userToken.getUser());
        } catch (SecurityException ex) {
            log.error("Error: " + ex);
            throw new SecurityException(ex);
        }
        return super.preHandle(request, response, handler);
    }
}