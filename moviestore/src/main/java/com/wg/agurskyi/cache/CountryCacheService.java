package com.wg.agurskyi.cache;

import com.wg.agurskyi.entity.Country;
import com.wg.agurskyi.service.CountryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class CountryCacheService {

    private final int FIXED_DELAY = 2*60*60*1000; // 2 hours
    private Map<Integer,Country> countryMap = new ConcurrentHashMap<>();

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private CountryService countryService;

    @Scheduled(fixedDelay=FIXED_DELAY)
    public void setAllCountries(){
        log.info("Sending request to get all countries for update cache");
        countryMap.clear();
        List<Country> countries = countryService.getAll();
        for (Country country : countries) {
            countryMap.put(country.getId(), country);
        }
        log.info("Countries are received. Count of countries {}", countries.size());
    }

    public Country getById(int id) {
        return countryMap.get(id);
    }
}
