package com.wg.agurskyi.cache;

import com.wg.agurskyi.entity.Movie;
import com.wg.agurskyi.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class MovieCacheService {

    private final String CRON = "0 0 0 * * *"; // every day at 00:00
    private Map<Integer, Movie> movieMap = new ConcurrentHashMap<>();

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private MovieService movieService;

    @Scheduled(cron = CRON)
    public void removeMovies() {
        log.info("Sending request to remove all marked movies from DB");
        int count = movieMap.size();
        for (Movie movie : movieMap.values()) {
            movieService.delete(movie.getId());
            movieMap.remove(movie.getId());
        }
        log.info("Movies were removed. Count of removed movies = {}", count);
    }

    public void add(Movie movie) {
        log.info("Add Movie {} to cache", movie);
        if (!movieMap.containsKey(movie.getId())) {
            movieMap.put(movie.getId(), movie);
            log.info("Movie was added to cache successfully");
        } else {
            log.info("Cache already contains Movie {}", movie);
        }
    }

    public void delete(Movie movie) {
        log.info("Delete Movie {} from cache", movie);
        if (movieMap.containsKey(movie.getId())) {
            movieMap.remove(movie.getId());
            log.info("Movie was deleted from cache successfully");
        } else {
            log.info("Cache does not contain Movie {}", movie);
        }
    }
}
