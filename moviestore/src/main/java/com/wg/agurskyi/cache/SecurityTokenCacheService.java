package com.wg.agurskyi.cache;

import com.wg.agurskyi.entity.UserToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class SecurityTokenCacheService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final int FIXED_DELAY = 60*1000; // 1 min
    private Map<String,UserToken> userTokenMap = new ConcurrentHashMap<>();

    public void add(UserToken userToken) {
        log.info("Add UserToken {} to cache", userToken);
        if (!userTokenMap.containsKey(userToken.getUser().getLogin())) {
            userTokenMap.put(userToken.getUser().getLogin(), userToken);
            log.info("UserToken was added successfully");
        } else {
            log.info("Cache already contains UserToken {}", userToken);
        }
    }

    @Scheduled(fixedDelay=FIXED_DELAY)
    private void removeExpiredToken(){
        int count = 0;
        LocalDateTime currentDate = LocalDateTime.now();
        log.info("Remove expired token from Security Token cache");
        for (UserToken userToken : userTokenMap.values()) {
            if (userToken.getExpirationDate().isBefore(currentDate)) {
                userTokenMap.remove(userToken.getUser().getLogin());
                count++;
            }
        }
        if (count > 0) {
            log.info("{} expired user tokens were removed successfully", count);
        } else {
            log.info("No expired user tokens in the cache");
        }
    }

    public UserToken getByLogin(String login) {
        log.info("get UserToken for login {} from cache", login);
        UserToken userToken = null;
        if (userTokenMap.containsKey(login)) {
            userToken = userTokenMap.get(login);
            log.info("UserToken was found in the cache");
        } else {
            log.info("Cache does not contain UserToken with login {}", login);
        }
        return userToken;
    }

    public UserToken getByToken(String token) {
        log.info("Get UserToken for token {} from cache", token);
        for (UserToken userToken : userTokenMap.values()) {
            if (userToken.getToken().equals(token)) {
                log.info("UserToken was found in the cache");
                return userToken;
            }
        }
        log.info("Cache does not contain UserToken with token {}", token);
        return null;
    }
}
