package com.wg.agurskyi.cache;

import com.wg.agurskyi.entity.Genre;
import com.wg.agurskyi.service.GenreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class GenreCacheService {

    private final int FIXED_DELAY = 2*60*60*1000; // 2 hours
    private Map<Integer,Genre> genreMap = new ConcurrentHashMap<>();

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private GenreService genreService;

    @Scheduled(fixedDelay=FIXED_DELAY)
    public void setAllGenres(){
        log.info("Sending request to get all genres for update cache");
        genreMap.clear();
        List<Genre> genres = genreService.getAll();
        for (Genre genre : genres) {
            genreMap.put(genre.getId(), genre);
        }
        log.info("Genres are received. Count of genres {}", genres.size());
    }

    public Genre getById(int id) {
        return genreMap.get(id);
    }
}
