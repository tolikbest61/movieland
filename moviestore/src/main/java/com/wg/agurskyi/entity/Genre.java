package com.wg.agurskyi.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wg.agurskyi.serializer.GenreSerializer;

@JsonSerialize(using = GenreSerializer.class)
public class Genre {
    private int id;
    private String name;

    public Genre() {
    }

    public Genre(int id) {
        this.id = id;
    }

    public Genre(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
