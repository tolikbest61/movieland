package com.wg.agurskyi.entity;

import java.util.List;

public class Movie {
    private int id;
    private String movieNameRu;
    private String movieNameEn;
    private int year;
    private String description;
    private double rating;
    private double price;
    private List<Genre> genres;
    private List<Country> countries;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMovieNameRu() {
        return movieNameRu;
    }

    public void setMovieNameRu(String movieNameRu) {
        this.movieNameRu = movieNameRu;
    }

    public String getMovieNameEn() {
        return movieNameEn;
    }

    public void setMovieNameEn(String movieNameEn) {
        this.movieNameEn = movieNameEn;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", movieNameRu='" + movieNameRu + '\'' +
                ", movieNameEn='" + movieNameEn + '\'' +
                ", year=" + year +
                ", description='" + description + '\'' +
                ", rating=" + rating +
                ", price=" + price +
                ", genres=" + genres +
                ", countries=" + countries +
                '}';
    }
}
