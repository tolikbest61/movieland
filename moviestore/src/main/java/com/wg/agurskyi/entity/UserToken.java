package com.wg.agurskyi.entity;

import java.time.LocalDateTime;

public final class UserToken {
    private User user;
    private String token;
    private LocalDateTime expirationDate;

    public UserToken(User user, String token, LocalDateTime expirationDate) {
        this.user = user;
        this.token = token;
        this.expirationDate = expirationDate;
    }

    public User getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    @Override
    public String toString() {
        return "UserToken{" +
                "user=" + user +
                ", token='" + token + '\'' +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
