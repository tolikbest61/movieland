package com.wg.agurskyi.entity;

public class Rating {

    private int authorId;
    private int movieId;
    private int rating;

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "authorId=" + authorId +
                ", movieId=" + movieId +
                ", rating=" + rating +
                '}';
    }
}
