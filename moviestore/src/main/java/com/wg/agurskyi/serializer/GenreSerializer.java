package com.wg.agurskyi.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.wg.agurskyi.entity.Genre;

import java.io.IOException;

public class GenreSerializer extends JsonSerializer<Genre> {
    @Override
    public void serialize(Genre genre, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        jgen.writeStringField("name", genre.getName());
        jgen.writeEndObject();
    }
}
