package com.wg.agurskyi.dao.jdbc;

import com.wg.agurskyi.dao.GenreDao;
import com.wg.agurskyi.dao.jdbc.mapper.GenreRowMapper;
import com.wg.agurskyi.entity.Genre;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcGenreDao implements GenreDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getGenreByIdSQL;

    @Autowired
    private String getListGenreByMovieIdSQL;

    @Autowired
    private String getAllGenresSQL;

    private final GenreRowMapper GENRE_ROW_MAPPER = new GenreRowMapper();

    @Override
    public Genre getById(int id) {
        log.info("Start query to get genre with genre_id {} from DB", id);
        long startTime = System.currentTimeMillis();
        Genre genre = jdbcTemplate.queryForObject(getGenreByIdSQL, new Object[]{id}, GENRE_ROW_MAPPER);
        log.info("Finish query to get genre with genre_id {} from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        return genre;
    }

    @Override
    public List<Genre> getByMovieId(int movieId) {
        log.info("Start query to get list of genres by movie_id {} from DB", movieId);
        long startTime = System.currentTimeMillis();
        List<Genre> genres = jdbcTemplate.query(getListGenreByMovieIdSQL, new Object[]{movieId}, GENRE_ROW_MAPPER);
        log.info("Finish query to get list of genres by move id {} from DB. It took {} ms", movieId, System.currentTimeMillis() - startTime);
        return genres;
    }

    @Override
    public List<Genre> getAll() {
        log.info("Start query to get all genres from DB");
        long startTime = System.currentTimeMillis();
        List<Genre> genres = jdbcTemplate.query(getAllGenresSQL, GENRE_ROW_MAPPER);
        log.info("Finish query to get all genres from DB. It took {} ms", System.currentTimeMillis() - startTime);
        return genres;
    }
}
