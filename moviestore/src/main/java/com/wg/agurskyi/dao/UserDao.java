package com.wg.agurskyi.dao;

import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.request.UserCredentialsRequest;

public interface UserDao {

    User getById(int id);

    User getByCredentials(UserCredentialsRequest userCredentialsRequest);
}
