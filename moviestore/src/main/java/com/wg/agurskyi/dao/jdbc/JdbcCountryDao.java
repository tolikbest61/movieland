package com.wg.agurskyi.dao.jdbc;

import com.wg.agurskyi.dao.CountryDao;
import com.wg.agurskyi.dao.jdbc.mapper.CountryRowMapper;
import com.wg.agurskyi.entity.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcCountryDao implements CountryDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getCountryByIdSQL;

    @Autowired
    private String getListCountryByMovieIdSQL;

    @Autowired
    private String getAllCountriesSQL;

    private final CountryRowMapper COUNTRY_ROW_MAPPER = new CountryRowMapper();

    @Override
    public Country getById(int id) {
        log.info("Start query to get country with country_id {} from DB", id);
        long startTime = System.currentTimeMillis();
        Country country = jdbcTemplate.queryForObject(getCountryByIdSQL, new Object[]{id}, COUNTRY_ROW_MAPPER);
        log.info("Finish query to get country with country_id {} from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        return country;
    }

    @Override
    public List<Country> getByMovieId(int movieId) {
        log.info("Start query to get list of country by movie_id {} from DB", movieId);
        long startTime = System.currentTimeMillis();
        List<Country> countries = jdbcTemplate.query(getListCountryByMovieIdSQL, new Object[]{movieId}, COUNTRY_ROW_MAPPER);
        log.info("Finish query to get list og country by move id {} from DB. It took {} ms", movieId, System.currentTimeMillis() - startTime);
        return countries;
    }

    @Override
    public List<Country> getAll() {
        log.info("Start query to get all countries from DB");
        long startTime = System.currentTimeMillis();
        List<Country> countries = jdbcTemplate.query(getAllCountriesSQL, COUNTRY_ROW_MAPPER);
        log.info("Finish query to get all countries from DB. It took {} ms", System.currentTimeMillis() - startTime);
        return countries;
    }
}
