package com.wg.agurskyi.dao;

import com.wg.agurskyi.entity.Movie;
import com.wg.agurskyi.request.MovieRequest;
import com.wg.agurskyi.request.MovieSearchRequest;

import java.util.List;

public interface MovieDao {

    Movie getById(int id);

    List<Movie> getAll(String ratingOrder, String priceOrder, int pageNumber);

    List<Movie> getAll();

    List<Movie> search(MovieSearchRequest movieSearchRequest);

    Movie add(MovieRequest movieRequest);

    Movie edit(MovieRequest movieRequest);

    void delete(int id);
}
