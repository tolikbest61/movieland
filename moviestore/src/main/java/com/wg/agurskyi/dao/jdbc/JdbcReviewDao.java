package com.wg.agurskyi.dao.jdbc;

import com.wg.agurskyi.dao.ReviewDao;
import com.wg.agurskyi.dao.jdbc.mapper.ReviewRowMapper;
import com.wg.agurskyi.entity.Review;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcReviewDao implements ReviewDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getReviewByIdSQL;

    @Autowired
    private String getListReviewByMovieIdSQL;

    @Autowired
    private String addReviewSQL;

    @Autowired
    private String removeReviewSQL;

    private final ReviewRowMapper REVIEW_ROW_MAPPER = new ReviewRowMapper();

    @Override
    public Review getById(int id) {
        log.info("Start query to get review by id {} from DB", id);
        long startTime = System.currentTimeMillis();
        Review review = jdbcTemplate.queryForObject(getReviewByIdSQL, new Object[]{id}, REVIEW_ROW_MAPPER);
        log.info("Finish query to get review by id {} from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        return review;
    }

    @Override
    public List<Review> getByMovieId(int movieId) {
        log.info("Start query to get list of reviews by movie_id {} from DB", movieId);
        long startTime = System.currentTimeMillis();
        List<Review> reviews = jdbcTemplate.query(getListReviewByMovieIdSQL, new Object[]{movieId}, REVIEW_ROW_MAPPER);
        log.info("Finish query to get list of reviews by move_id {} from DB. It took {} ms", movieId, System.currentTimeMillis() - startTime);
        return reviews;
    }

    @Override
    public void add(Review review) {
        try {
            log.info("Start query to add new review {} into DB ", review);
            long startTime = System.currentTimeMillis();
            jdbcTemplate.update(addReviewSQL, new Object[]{review.getMovie().getId(),review.getUser().getId(), review.getComment()});
            log.info("Review {} was added to DB. It took {} ms", review, System.currentTimeMillis() - startTime);
        } catch (Exception ex) {
            log.warn("Cannot save review to DB " + ex.getMessage());
        }
    }

    @Override
    public void remove(int id) {
        try {
            log.info("Start query to remove review with id={} from DB ", id);
            long startTime = System.currentTimeMillis();
            jdbcTemplate.update(removeReviewSQL, new Object[]{id});
            log.info("Review {} was removed from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        } catch (Exception ex) {
            String message = "Cannot remove review from DB. Error:" + ex.getMessage();
            log.warn(message);
            throw new RuntimeException(message);
        }
    }
}
