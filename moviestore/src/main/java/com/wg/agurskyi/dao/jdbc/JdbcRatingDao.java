package com.wg.agurskyi.dao.jdbc;

import com.wg.agurskyi.dao.RatingDao;
import com.wg.agurskyi.entity.Rating;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class JdbcRatingDao implements RatingDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private String upsertRatingSQL;

    @Override
    public void addOrModify(Rating rating) {
        try {
            log.info("Start query to add new review {} into DB ", rating);
            long startTime = System.currentTimeMillis();
            Map<String,Object> namedParameters = new HashMap();
            namedParameters.put("movie_id", rating.getMovieId());
            namedParameters.put("user_id", rating.getAuthorId());
            namedParameters.put("rating", rating.getRating());
            namedParameterJdbcTemplate.update(upsertRatingSQL,namedParameters);
            log.info("Rating {} was added to DB. It took {} ms", rating, System.currentTimeMillis() - startTime);
        } catch (Exception ex) {
            String message = "Cannot add or modify rating in DB. Error:" + ex.getMessage();
            log.warn(message);
            throw new RuntimeException(message);
        }
    }

}
