package com.wg.agurskyi.dao.jdbc.mapper;

import com.wg.agurskyi.entity.Movie;
import com.wg.agurskyi.entity.Review;
import com.wg.agurskyi.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReviewRowMapper implements RowMapper<Review> {

    @Override
    public Review mapRow(ResultSet rs, int rowNum) throws SQLException {
        Review review = new Review();
        review.setId(rs.getInt("review_id"));
        Movie movie = new Movie();
        movie.setId(rs.getInt("movie_id"));
        review.setMovie(movie);
        User user = new User();
        user.setId(rs.getInt("user_id"));
        review.setUser(user);
        review.setComment(rs.getString("comment"));
        return review;
    }
}