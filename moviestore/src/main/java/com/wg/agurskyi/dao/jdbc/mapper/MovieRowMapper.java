package com.wg.agurskyi.dao.jdbc.mapper;

import com.wg.agurskyi.entity.Country;
import com.wg.agurskyi.entity.Genre;
import com.wg.agurskyi.entity.Movie;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.wg.agurskyi.util.Constant.COMMA_DELIMITER;

public class MovieRowMapper implements RowMapper<Movie> {

    @Override
    public Movie mapRow(ResultSet rs, int rowNum) throws SQLException {
        Movie movie = new Movie();
        movie.setId(rs.getInt("movie_id"));
        movie.setMovieNameRu(rs.getString("movie_name_ru"));
        movie.setMovieNameEn(rs.getString("movie_name_en"));
        movie.setYear(rs.getInt("year"));
        movie.setDescription(rs.getString("description"));
        movie.setRating(rs.getDouble("rating"));
        movie.setPrice(rs.getDouble("price"));
        String genres = rs.getString("genre_list");
        if (genres != null) {
            List<Genre> genreList = new ArrayList<>();
            for (int i = 0; i < genres.split(COMMA_DELIMITER).length; i++) {
                Genre genre = new Genre(Integer.parseInt(genres.split(COMMA_DELIMITER)[i]));
                genreList.add(genre);
            }
            movie.setGenres(genreList);
        }
        String countries = rs.getString("country_list");
        if (countries != null) {
            List<Country> countryList = new ArrayList<>();
            for (int i = 0; i < countries.split(COMMA_DELIMITER).length; i++) {
                Country country = new Country(Integer.parseInt(countries.split(COMMA_DELIMITER)[i]));
                countryList.add(country);
            }
            movie.setCountries(countryList);
        }
        return movie;
    }
}
