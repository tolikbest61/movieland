package com.wg.agurskyi.dao;

import com.wg.agurskyi.entity.Review;

import java.util.List;

public interface ReviewDao {

    Review getById(int id);

    List<Review> getByMovieId(int movieId);

    void add(Review review);

    void remove(int id);
}
