package com.wg.agurskyi.dao.jdbc;

import com.wg.agurskyi.dao.MovieDao;
import com.wg.agurskyi.dao.jdbc.mapper.MovieRowMapper;
import com.wg.agurskyi.entity.Country;
import com.wg.agurskyi.entity.Genre;
import com.wg.agurskyi.entity.Movie;
import com.wg.agurskyi.request.MovieRequest;
import com.wg.agurskyi.request.MovieSearchRequest;
import com.wg.agurskyi.util.SqlGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class JdbcMovieDao implements MovieDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private String getAllMovieSQL;

    @Autowired
    private String getMovieByIdSQL;

    @Autowired
    private String insertMovieSQL;

    @Autowired
    private String updateMovieSQL;

    @Autowired
    private String insetLinkMovieCountrySQL;

    @Autowired
    private String deleteLinkMovieCountrySQL;

    @Autowired
    private String insetLinkMovieGenreSQL;

    @Autowired
    private String deleteLinkMovieGenreSQL;

    @Autowired
    private String deleteMovieSQL;

    private final MovieRowMapper MOVIE_ROW_MAPPER = new MovieRowMapper();

    @Override
    public Movie getById(int id) {
        try {
            log.info("Start query to get movie with movie_id {} from DB", id);
            long startTime = System.currentTimeMillis();
            Movie movie = jdbcTemplate.queryForObject(getMovieByIdSQL, new Object[]{id}, MOVIE_ROW_MAPPER);
            log.info("Finish query to get movie with movie_id {} from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
            return movie;
        } catch (Exception ex) {
            String message = "Cannot get movie with id=" + id + " from DB. Error:" + ex.getMessage();
            log.warn(message);
            throw new RuntimeException(message);
        }
    }

    @Override
    public List<Movie> getAll(String ratingOrder, String priceOrder, int pageNumber) {
        log.info("Get all movies from table on page={}", pageNumber);
        List<Movie> movies = jdbcTemplate.query(SqlGenerator.buildSortingSql(getAllMovieSQL, ratingOrder,
                priceOrder, pageNumber), MOVIE_ROW_MAPPER);
        log.info("Finish query to get all movies from table on page={}", pageNumber);
        return movies;
    }

    @Override
    public List<Movie> getAll() {
        log.info("Get all movies from table");
        List<Movie> movies = jdbcTemplate.query(getAllMovieSQL, MOVIE_ROW_MAPPER);
        log.info("Finish query to get all movies from table");
        return movies;
    }

    @Override
    public List<Movie> search(MovieSearchRequest movieSearchRequest) {
        log.info("Start search movies");
        List<Movie> movies = jdbcTemplate.query(SqlGenerator.buildSqlFilter(getAllMovieSQL, movieSearchRequest), MOVIE_ROW_MAPPER);
        log.info("Finish search movies in DB. Count is ", movies.size());
        return movies;
    }

    @Override
    public Movie add(MovieRequest movieRequest) {
        try {
            log.info("Start query to add new moview {} into DB ", movieRequest);
            long startTime = System.currentTimeMillis();
            Map<String, Object> namedParameters = new HashMap();
            namedParameters.put("movie_name_ru", movieRequest.getMovieNameRu());
            namedParameters.put("movie_name_en", movieRequest.getMovieNameEn());
            namedParameters.put("year", movieRequest.getYear());
            namedParameters.put("description", movieRequest.getDescription());
            namedParameters.put("price", movieRequest.getPrice());
            int movie_id = namedParameterJdbcTemplate.queryForObject(insertMovieSQL, namedParameters, Integer.class);

            List<Map<String, Object>> batchValues = new ArrayList<>(movieRequest.getCountries().size());
            for (Country country : movieRequest.getCountries()) {
                batchValues.add(new MapSqlParameterSource("movie_id", movie_id).addValue("country_id", country.getId()).getValues());
            }
            namedParameterJdbcTemplate.batchUpdate(insetLinkMovieCountrySQL, batchValues.toArray(new Map[movieRequest.getCountries().size()]));

            batchValues = new ArrayList<>(movieRequest.getGenres().size());
            for (Genre genre : movieRequest.getGenres()) {
                batchValues.add(new MapSqlParameterSource("movie_id", movie_id).addValue("genre_id", genre.getId()).getValues());
            }
            namedParameterJdbcTemplate.batchUpdate(insetLinkMovieGenreSQL, batchValues.toArray(new Map[movieRequest.getGenres().size()]));
            log.info("Movie {} was added to DB. Id = {}. It took {} ms", movieRequest, movie_id, System.currentTimeMillis() - startTime);
            return getById(movie_id);
        } catch (Exception ex) {
            String message = "Cannot add or modify movie in DB. Error:" + ex.getMessage();
            log.warn(message);
            throw new RuntimeException(message);
        }
    }


    @Override
    public Movie edit(MovieRequest movieRequest) {
        try {
            log.info("Start query to edit moview {} into DB ", movieRequest);
            long startTime = System.currentTimeMillis();
            Map<String, Object> namedParameters = new HashMap();
            namedParameters.put("movie_name_ru", movieRequest.getMovieNameRu());
            namedParameters.put("movie_name_en", movieRequest.getMovieNameEn());
            namedParameters.put("year", movieRequest.getYear());
            namedParameters.put("description", movieRequest.getDescription());
            namedParameters.put("price", movieRequest.getPrice());
            int movie_id = namedParameterJdbcTemplate.queryForObject(updateMovieSQL, namedParameters, Integer.class);

            namedParameters = new HashMap();
            namedParameters.put("movie_id", movie_id);
            namedParameterJdbcTemplate.update(deleteLinkMovieCountrySQL, namedParameters);
            namedParameterJdbcTemplate.update(deleteLinkMovieGenreSQL, namedParameters);

            List<Map<String, Object>> batchValues = new ArrayList<>(movieRequest.getCountries().size());
            for (Country country : movieRequest.getCountries()) {
                batchValues.add(new MapSqlParameterSource("movie_id", movie_id).addValue("country_id", country.getId()).getValues());
            }
            namedParameterJdbcTemplate.batchUpdate(insetLinkMovieCountrySQL, batchValues.toArray(new Map[movieRequest.getCountries().size()]));

            batchValues = new ArrayList<>(movieRequest.getGenres().size());
            for (Genre genre : movieRequest.getGenres()) {
                batchValues.add(new MapSqlParameterSource("movie_id", movie_id).addValue("genre_id", genre.getId()).getValues());
            }
            namedParameterJdbcTemplate.batchUpdate(insetLinkMovieGenreSQL, batchValues.toArray(new Map[movieRequest.getGenres().size()]));
            log.info("Movie {} was added to DB. Id = {}. It took {} ms", movieRequest, movie_id, System.currentTimeMillis() - startTime);
            return getById(movie_id);
        } catch (Exception ex) {
            String message = "Cannot add or modify movie in DB. Error:" + ex.getMessage();
            log.warn(message);
            throw new RuntimeException(message);
        }
    }


    @Override
    public void delete(int id) {
        try {
            log.info("Start query to delete movie with id={} from DB ", id);
            long startTime = System.currentTimeMillis();
            Map<String, Object> namedParameters = new HashMap();
            namedParameters = new HashMap();
            namedParameters.put("movie_id", id);
            namedParameterJdbcTemplate.update(deleteLinkMovieCountrySQL, namedParameters);
            namedParameterJdbcTemplate.update(deleteLinkMovieGenreSQL, namedParameters);
            namedParameterJdbcTemplate.update(deleteMovieSQL, namedParameters);
            log.info("Movie {} was deleted from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        } catch (Exception ex) {
            String message = "Cannot delete movie from DB. Error:" + ex.getMessage();
            log.warn(message);
            throw new RuntimeException(message);
        }
    }
}
