package com.wg.agurskyi.dao;

import com.wg.agurskyi.entity.Genre;
import java.util.List;

public interface GenreDao {

    Genre getById(int id);

    List<Genre> getByMovieId(int movieId);

    List<Genre> getAll();
}
