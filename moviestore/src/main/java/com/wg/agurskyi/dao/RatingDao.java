package com.wg.agurskyi.dao;

import com.wg.agurskyi.entity.Rating;

public interface RatingDao {

    void addOrModify(Rating rating);
}
