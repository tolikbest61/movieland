package com.wg.agurskyi.dao.jdbc.mapper;

import com.wg.agurskyi.entity.Country;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CountryRowMapper implements RowMapper<Country> {

    @Override
    public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Country(rs.getInt("country_id"),rs.getString("country_name"));
    }
}
