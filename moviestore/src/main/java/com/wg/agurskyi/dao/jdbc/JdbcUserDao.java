package com.wg.agurskyi.dao.jdbc;

import com.wg.agurskyi.dao.UserDao;
import com.wg.agurskyi.dao.jdbc.mapper.UserRowMapper;
import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.request.UserCredentialsRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcUserDao implements UserDao {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getUserByIdSQL;

    @Autowired
    private String getUserByLoginSQL;

    private final UserRowMapper USER_ROW_MAPPER = new UserRowMapper();

    @Override
    public User getById(int id) {
        log.info("Start query to get user with user_id {} from DB", id);
        long startTime = System.currentTimeMillis();
        User user = jdbcTemplate.queryForObject(getUserByIdSQL, new Object[]{id}, USER_ROW_MAPPER);
        log.info("Finish query to get user with user_id {} from DB. It took {} ms", id, System.currentTimeMillis() - startTime);
        return user;
    }

    @Override
    public User getByCredentials(UserCredentialsRequest userCredentialsRequest) {
        log.info("Start query to get user with login {} from DB", userCredentialsRequest.getLogin());
        long startTime = System.currentTimeMillis();
        User user;
        try {
            user = jdbcTemplate.queryForObject(getUserByLoginSQL, new Object[]{userCredentialsRequest.getLogin()}, USER_ROW_MAPPER);
        }
        catch (Exception ex) {
            log.warn("User with login {} not found in DB. It took [] ms", userCredentialsRequest.getLogin(), System.currentTimeMillis() - startTime);
            return null;
        }
        log.info("Finish query to get user with login {} from DB. It took {} ms", userCredentialsRequest.getLogin(), System.currentTimeMillis() - startTime);
        return user;
    }
}
