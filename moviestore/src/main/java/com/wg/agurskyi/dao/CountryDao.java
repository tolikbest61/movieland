package com.wg.agurskyi.dao;

import com.wg.agurskyi.entity.Country;
import java.util.List;

public interface CountryDao {

    Country getById(int id);

    List<Country> getByMovieId(int movieId);

    List<Country> getAll();
}
