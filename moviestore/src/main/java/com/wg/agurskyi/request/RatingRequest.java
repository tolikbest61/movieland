package com.wg.agurskyi.request;

public class RatingRequest {

    private int movieId;
    private int rating;

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Rating{" +
                ", movieId=" + movieId +
                ", rating=" + rating +
                '}';
    }
}
