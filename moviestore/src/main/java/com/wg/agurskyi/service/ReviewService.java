package com.wg.agurskyi.service;

import com.wg.agurskyi.entity.Review;
import com.wg.agurskyi.request.ReviewRequest;

import java.util.List;

public interface ReviewService {

    Review getById(int id);

    List<Review> getByMovieId(int id);

    Review addReview(ReviewRequest reviewRequest);

    void removeReview(int id);
}
