package com.wg.agurskyi.service.impl;

import com.wg.agurskyi.dao.GenreDao;
import com.wg.agurskyi.entity.Genre;
import com.wg.agurskyi.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService{

    @Autowired
    private GenreDao genreDao;

    @Override
    public Genre getById(int id) {
        return genreDao.getById(id);
    }

    public List<Genre> getByMovieId(int movieId) {
        return genreDao.getByMovieId(movieId);
    }

    public List<Genre> getAll() {
        return genreDao.getAll();
    }
}
