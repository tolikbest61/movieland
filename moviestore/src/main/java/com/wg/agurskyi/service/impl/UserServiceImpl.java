package com.wg.agurskyi.service.impl;

import com.wg.agurskyi.dao.UserDao;
import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.entity.UserToken;
import com.wg.agurskyi.request.UserCredentialsRequest;
import com.wg.agurskyi.service.SecurityService;
import com.wg.agurskyi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private SecurityService securityService;

    @Override
    public User getById(int id) {
        return userDao.getById(id);
    }

    @Override
    public UserToken authoriseUser(UserCredentialsRequest userCredentialsRequest) {
        User user = userDao.getByCredentials(userCredentialsRequest);
        if (user == null) {
            throw new SecurityException("Login is invalid");
        }
        if (!securityService.isValidUser(user, userCredentialsRequest)) {
            throw new SecurityException("Password is invalid");
        }
        UserToken userToken = securityService.registerUser(user);
        return userToken;
    }
}
