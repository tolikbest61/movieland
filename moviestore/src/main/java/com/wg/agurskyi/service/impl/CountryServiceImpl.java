package com.wg.agurskyi.service.impl;

import com.wg.agurskyi.dao.CountryDao;
import com.wg.agurskyi.entity.Country;
import com.wg.agurskyi.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryDao countryDao;

    @Override
    public Country getById(int id) {
        return countryDao.getById(id);
    }

    @Override
    public List<Country> getByMovieId(int movieId) {
        return countryDao.getByMovieId(movieId);
    }

    @Override
    public List<Country> getAll() {
        return countryDao.getAll();
    }
}
