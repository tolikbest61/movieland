package com.wg.agurskyi.service.impl;

import com.wg.agurskyi.dao.RatingDao;
import com.wg.agurskyi.entity.Rating;
import com.wg.agurskyi.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements RatingService {

    @Autowired
    private RatingDao ratingDao;

    @Override
    public void addOrModify(Rating rating) {
        try {
            ratingDao.addOrModify(rating);
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }
}
