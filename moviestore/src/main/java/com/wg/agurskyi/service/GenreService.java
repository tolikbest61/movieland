package com.wg.agurskyi.service;

import com.wg.agurskyi.entity.Genre;

import java.util.List;

public interface GenreService {

    Genre getById(int id);

    List<Genre> getByMovieId(int movieId);

    List<Genre> getAll();
}
