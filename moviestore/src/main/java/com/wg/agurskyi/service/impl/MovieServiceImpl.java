package com.wg.agurskyi.service.impl;

import com.wg.agurskyi.cache.CountryCacheService;
import com.wg.agurskyi.cache.GenreCacheService;
import com.wg.agurskyi.cache.MovieCacheService;
import com.wg.agurskyi.dao.MovieDao;
import com.wg.agurskyi.entity.Country;
import com.wg.agurskyi.entity.Genre;
import com.wg.agurskyi.entity.Movie;
import com.wg.agurskyi.request.MovieRequest;
import com.wg.agurskyi.request.MovieSearchRequest;
import com.wg.agurskyi.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieDao movieDao;

    @Autowired
    private GenreCacheService genreCacheService;

    @Autowired
    private CountryCacheService countryCacheService;

    @Autowired
    private MovieCacheService movieCacheService;

    @Override
    public Movie getById(int movieId) {
        Movie movie = movieDao.getById(movieId);
        movie.setCountries(getCountryFromCacheByMovie(movie));
        movie.setGenres(getGenreFromCacheByMovie(movie));
        return movie;
    }

    @Override
    public List<Movie> getAll(String ratingOrder, String priceOrder, int pageNumber) {
        List<Movie> movies = movieDao.getAll(ratingOrder, priceOrder, pageNumber);
        for (Movie movie : movies) {
            movie.setGenres(getGenreFromCacheByMovie(movie));
        }
        return movies;
    }

    @Override
    public List<Movie> search(MovieSearchRequest movieSearchRequest) {
        List<Movie> movies = movieDao.search(movieSearchRequest);
        for (Movie movie : movies) {
            movie.setGenres(getGenreFromCacheByMovie(movie));
        }
        return movies;
    }

    @Override
    public Movie add(MovieRequest movieRequest) {
        if (searchMovieByMovieRequest(movieRequest).size() == 0) {
            return movieDao.add(movieRequest);
        } else {
            throw new RuntimeException("Movie with name '" + movieRequest.getMovieNameEn() + "' already is present in DB");
        }
    }

    @Override
    public Movie edit(MovieRequest movieRequest) {
        if (searchMovieByMovieRequest(movieRequest).size() > 0) {
            return movieDao.edit(movieRequest);
        } else {
            throw new RuntimeException("Movie with name '" + movieRequest.getMovieNameEn() + "' is absent in DB");
        }
    }

    @Override
    public Movie markForDelete(int movieId) {
        Movie movie = getById(movieId);
        movieCacheService.add(movie);
        return movie;
    }

    @Override
    public Movie unmarkForDelete(int movieId) {
        Movie movie = getById(movieId);
        movieCacheService.delete(movie);
        return movie;
    }

    @Override
    public void delete(int movieId) {
        try {
            movieDao.delete(movieId);
        } catch (Exception ex) {
            throw new RuntimeException("Cannot remove review. Error:" + ex.getMessage());
        }
    }

    private List<Genre> getGenreFromCacheByMovie(Movie movie) {
        return movie.getGenres().stream().map(Genre -> genreCacheService.getById(Genre.getId())).collect(Collectors.toList());
    }

    private List<Country> getCountryFromCacheByMovie(Movie movie) {
        return movie.getCountries().stream().map(Country -> countryCacheService.getById(Country.getId())).collect(Collectors.toList());
    }

    private List<Movie> searchMovieByMovieRequest(MovieRequest movieRequest) {
        MovieSearchRequest movieSearchRequest = new MovieSearchRequest();
        movieSearchRequest.setName(movieRequest.getMovieNameEn());
        return search(movieSearchRequest);
    }
}
