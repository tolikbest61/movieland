package com.wg.agurskyi.service.impl;

import com.wg.agurskyi.cache.SecurityTokenCacheService;
import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.entity.UserToken;
import com.wg.agurskyi.request.UserCredentialsRequest;
import com.wg.agurskyi.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class SecurityServiceImpl implements SecurityService {

    private final long LIVE_INTERVAL = 2*60*60; //2 hours

    @Autowired
    private SecurityTokenCacheService securityTokenCacheService;

    @Override
    public boolean isValidUser(User user, UserCredentialsRequest userCredentialsRequest) {
        return user.getPassword().equals(userCredentialsRequest.getPassword());
    }

    @Override
    public UserToken registerUser(User user) {
        UserToken userToken = securityTokenCacheService.getByLogin(user.getLogin());
        if (userToken == null) {
            userToken = new UserToken(user, generateToken(), LocalDateTime.now().plusSeconds(LIVE_INTERVAL));
            securityTokenCacheService.add(userToken);
        }
        return userToken;
    }

    @Override
    public UserToken validateToken(String token) {
        UserToken userToken = securityTokenCacheService.getByToken(token);
        if (userToken == null) {
            throw new SecurityException("Token is not valid");
        }
        return userToken;
    }

    private String generateToken() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
