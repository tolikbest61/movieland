package com.wg.agurskyi.service;

import com.wg.agurskyi.entity.Country;

import java.util.List;

public interface CountryService {

    Country getById(int id);

    List<Country> getByMovieId(int movieId);

    List<Country> getAll();
}
