package com.wg.agurskyi.service;

import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.entity.UserToken;
import com.wg.agurskyi.request.UserCredentialsRequest;

public interface UserService {

    User getById(int id);

    UserToken authoriseUser(UserCredentialsRequest userCredentialsRequest);
}
