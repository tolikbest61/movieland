package com.wg.agurskyi.service;

import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.request.MovieRequest;
import com.wg.agurskyi.request.MovieSearchRequest;
import com.wg.agurskyi.entity.Movie;
import java.util.List;

public interface MovieService {

    List<Movie> getAll(String ratingOrder, String priceOrder, int pageNumber);

    Movie getById(int movieId);

    List<Movie> search(MovieSearchRequest movieSearchRequest);

    Movie add(MovieRequest movieRequest);

    Movie edit(MovieRequest movieRequest);

    Movie markForDelete(int movieId);

    Movie unmarkForDelete(int movieId);

    void delete(int movieId);
}

