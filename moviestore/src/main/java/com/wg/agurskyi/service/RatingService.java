package com.wg.agurskyi.service;

import com.wg.agurskyi.entity.Rating;

public interface RatingService {

    void addOrModify(Rating rating);
}
