package com.wg.agurskyi.service.impl;

import com.wg.agurskyi.dao.ReviewDao;
import com.wg.agurskyi.entity.Movie;
import com.wg.agurskyi.entity.Review;
import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.request.ReviewRequest;
import com.wg.agurskyi.service.MovieService;
import com.wg.agurskyi.service.ReviewService;
import com.wg.agurskyi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewDao reviewDao;

    @Autowired
    private UserService userService;

    @Autowired
    private MovieService movieService;

    @Override
    public Review getById(int id) {
        return reviewDao.getById(id);
    }

    @Override
    public List<Review> getByMovieId(int movieId) {
        List<Review> reviews =reviewDao.getByMovieId(movieId);
        for (Review review : reviews) {
            review.setUser(userService.getById(review.getUser().getId()));
        }
        return reviews;
    }

    @Override
    public Review addReview(ReviewRequest reviewRequest) {
        Movie movie = movieService.getById(reviewRequest.getMovieId());
        User user = userService.getById(reviewRequest.getAuthorId());
        if (movie != null && user != null) {
            try {
                Review review = new Review();
                review.setMovie(movie);
                review.setUser(user);
                review.setComment(reviewRequest.getReview());
                reviewDao.add(review);
                return review;
            } catch (Exception ex) {
                throw new RuntimeException("Cannot add review to DB. Error: " + ex.getMessage());
            }
        } else {
            throw new RuntimeException("Cannot add review to DB. Movie or User not found in DB");
        }
    }

    @Override
    public void removeReview(int id) {
        try {
            reviewDao.remove(id);
        } catch (Exception ex) {
            throw new RuntimeException("Cannot remove review. Error:" + ex.getMessage());
        }
    }
}
