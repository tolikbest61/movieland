package com.wg.agurskyi.service;

import com.wg.agurskyi.entity.User;
import com.wg.agurskyi.entity.UserToken;
import com.wg.agurskyi.request.UserCredentialsRequest;

public interface SecurityService {

    boolean isValidUser(User user, UserCredentialsRequest userCredentialsRequest);

    UserToken registerUser(User user);

    UserToken validateToken(String token);
}
