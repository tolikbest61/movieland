package com.wg.agurskyi.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wg.agurskyi.entity.Country;
import com.wg.agurskyi.entity.Genre;
import com.wg.agurskyi.entity.Review;
import com.wg.agurskyi.serializer.UserSerializer;

import java.util.List;

public class DetailMovieDto {

    private String movieNameRu;
    private String movieNameOriginal;
    private int year;
    private double rating;
    private List<Genre> genre;
    private List<Country> country;
    private List<Review> review;
    private String description;

    public String getMovieNameRu() {
        return movieNameRu;
    }

    public void setMovieNameRu(String movieNameRu) {
        this.movieNameRu = movieNameRu;
    }

    public String getMovieNameOriginal() {
        return movieNameOriginal;
    }

    public void setMovieNameOriginal(String movieNameOriginal) {
        this.movieNameOriginal = movieNameOriginal;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public List<Genre> getGenre() {
        return genre;
    }

    public void setGenre(List<Genre> genre) {
        this.genre = genre;
    }

    public List<Country> getCountry() {
        return country;
    }

    public void setCountry(List<Country> country) {
        this.country = country;
    }

    public List<Review> getReview() {
        return review;
    }

    public void setReview(List<Review> review) {
        this.review = review;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "DetailMovieDto{" +
                "movieNameRu='" + movieNameRu + '\'' +
                ", movieNameOriginal='" + movieNameOriginal + '\'' +
                ", year=" + year +
                ", rating=" + rating +
                ", genre=" + genre +
                ", country=" + country +
                ", review=" + review +
                ", description='" + description + '\'' +
                '}';
    }
}
