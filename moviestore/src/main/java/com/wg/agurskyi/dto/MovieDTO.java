package com.wg.agurskyi.dto;

import com.wg.agurskyi.entity.Genre;
import java.util.List;

public class MovieDto {

    private String movieNameRu;
    private String movieNameOriginal;
    private int year;
    private double rating;
    private List<Genre> genres;

    public String getMovieNameRu() {
        return movieNameRu;
    }

    public void setMovieNameRu(String movieNameRu) {
        this.movieNameRu = movieNameRu;
    }

    public String getMovieNameOriginal() {
        return movieNameOriginal;
    }

    public void setMovieNameOriginal(String movieNameOriginal) {
        this.movieNameOriginal = movieNameOriginal;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    @Override
    public String toString() {
        return "MovieDto{" +
                "movieNameRu='" + movieNameRu + '\'' +
                ", movieNameOriginal='" + movieNameOriginal + '\'' +
                ", year=" + year +
                ", rating=" + rating +
                ", genres=" + genres +
                '}';
    }
}
