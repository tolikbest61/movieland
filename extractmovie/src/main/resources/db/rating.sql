CREATE TABLE rating
(
    movie_id integer REFERENCES movie (movie_id),
    user_id integer REFERENCES users (user_id),
    rating integer
);