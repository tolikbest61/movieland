CREATE TABLE movie
(
    movie_id SERIAL PRIMARY KEY,
    movie_name_ru varchar(100),
    movie_name_en varchar(100),
    year integer,
    description text,
    price decimal(6,2)
);