CREATE TABLE review
(
  review_id SERIAL PRIMARY KEY,
  movie_id integer REFERENCES movie (movie_id),
  user_id integer REFERENCES users (user_id),
  comment text,
  date_insert date not null default CURRENT_DATE
);