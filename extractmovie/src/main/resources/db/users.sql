CREATE TABLE users
(
  user_id SERIAL PRIMARY KEY,
  first_name varchar(50),
  last_name varchar(50),
  login varchar(100),
  password varchar(50),
  role integer DEFAULT 1
);