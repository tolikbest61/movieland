CREATE TABLE movie_country
(
  movie_id integer REFERENCES movie (movie_id),
  country_id integer REFERENCES country (country_id)
);