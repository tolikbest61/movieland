CREATE TABLE movie_genre
(
  movie_id integer REFERENCES movie(movie_id),
  genre_id integer REFERENCES genre(genre_id)
);