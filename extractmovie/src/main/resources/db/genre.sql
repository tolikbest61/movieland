CREATE TABLE genre
(
  genre_id SERIAL PRIMARY KEY,
  genre_name varchar(50)
);