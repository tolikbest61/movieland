package com.wg.agurskyi;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;


public class Main {

    static final String QUERY_DELIMITER = ";";
    static final String BLANK_DELIMITER = " ";


    public static void main(String args[]) {
        cleanDatabase();
        parseGenre();
        parseUser();
        parseMovie();
        parseReview();
    }

    public static void executeScript(String[] command) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/postgres",
                            "postgres", "forever");
            stmt = c.createStatement();
            for (String sql : command) {
                stmt.executeUpdate(sql);
            }
            stmt.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ":" + e.getMessage());
            System.exit(0);
        }
    }

    public static void testConnection() {
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/postgres",
                            "postgres", "forever");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }

    public static void cleanDatabase() {
        String command = "";
        String list_tables = "country;genre;movie;movie_country;movie_genre;review;rating;users";
        for (String table : list_tables.split(QUERY_DELIMITER)) {
            command += "TRUNCATE " + table + " RESTART IDENTITY CASCADE;";
        }
        executeScript(command.split(QUERY_DELIMITER));
    }

    public static void parseGenre() {

        System.out.println("******insert into genre table******");

        Path file = Paths.get("src\\main\\resources\\source\\genre.txt");
        String fill_genre = "";
        try (InputStream in = Files.newInputStream(file);
             BufferedReader reader =
                     new BufferedReader(new InputStreamReader(in))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                fill_genre += "insert into genre (genre_name) VALUES ('" + line + "');\n";
            }
            System.out.println(fill_genre);
            executeScript(fill_genre.split(QUERY_DELIMITER));
        } catch (IOException x) {
            System.err.println(x);
        }
    }

    public static void parseUser() {

        System.out.println("******insert into users table******");

        Path file = Paths.get("src\\main\\resources\\source\\user.txt");
        String fill_user = "";
        try (InputStream in = Files.newInputStream(file);
             BufferedReader reader =
                     new BufferedReader(new InputStreamReader(in))) {
            String line = null;
            int i = 0;
            String[] list = new String[3];
            while ((line = reader.readLine()) != null) {
                if (line.length() > 0) {
                    list[i++] = line;
                    if (i > list.length - 1) {
                        fill_user += "insert into users (first_name, last_name, login, password, role) VALUES ('" + list[0].split(BLANK_DELIMITER)[0]
                                + "', '" + list[0].split(BLANK_DELIMITER)[1] + "', '" + list[1] + "', '" + list[2] + "',1);\n";
                        i = 0;
                    }
                }
            }
            fill_user += "insert into users(first_name, last_name,login,password,role) values('Анатолій','Гурський','tolikbest61@gmail.com','forever',0);\n";
            System.out.println(fill_user);
            executeScript(fill_user.split(QUERY_DELIMITER));
        } catch (IOException x) {
            System.err.println(x);
        }
    }

    public static void parseMovie() {

        Path file = Paths.get("src\\main\\resources\\source\\movie.txt");
        String fill_country = "";
        String fill_movie = "";
        String fill_movie_country = "";
        String fill_movie_genre = "";
        try (InputStream in = Files.newInputStream(file);
             BufferedReader reader =
                     new BufferedReader(new InputStreamReader(in))) {
            String line = null;
            int i = 0;
            String[] list = new String[7];
            Set<String> countries = new HashSet<>();
            while ((line = reader.readLine()) != null) {
                if (line.length() > 0) {
                    list[i++] = line;
                    if (i > list.length - 1) {

                        String movie_name_ru = list[0].split("/")[0].replace("'", "''");
                        String movie_name_en = list[0].split("/")[1].replace("'", "''");
                        int year = Integer.parseInt(list[1]);
                        String description = list[4];
                        double rating = Double.parseDouble(list[5].split(":")[1].trim());
                        double price = Double.parseDouble(list[6].split(":")[1].trim());

                        fill_movie += "insert into movie (movie_name_ru, movie_name_en, year, description, price) values ('" + movie_name_ru + "','" + movie_name_en + "'," + year
                                + ",'" + description + "'," + price + ");\n";

                        String[] list_country = list[2].split(",");
                        for (int j = 0; j < list_country.length; j++) {
                            String country_name = list_country[j].trim();
                            countries.add(country_name);
                            fill_movie_country += "insert into movie_country(movie_id, country_id) values ((select movie_id from movie where movie_name_en = '" + movie_name_en +
                                    "'),(select country_id from country where country_name = '" + country_name + "'));\n";
                        }

                        String[] list_genre = list[3].split(",");
                        for (int j = 0; j < list_genre.length; j++) {
                            String genre_name = list_genre[j].trim();
                            fill_movie_genre += "insert into movie_genre(movie_id, genre_id) values ((select movie_id from movie where movie_name_en = '" + movie_name_en +
                                    "'),(select genre_id from genre where genre_name = '" + genre_name + "'));\n";
                        }
                        i = 0;
                    }
                }
            }
            for (String country : countries) {
                fill_country += "insert into country(country_name) values ('" + country + "');\n";
            }
            System.out.println("******insert into country table******");
            System.out.println(fill_country);
            executeScript(fill_country.split(QUERY_DELIMITER));
            System.out.println("******insert into movie table******");
            System.out.println(fill_movie);
            executeScript(fill_movie.split(QUERY_DELIMITER));
            System.out.println("******insert into movie_country table******");
            System.out.println(fill_movie_country);
            executeScript(fill_movie_country.split(QUERY_DELIMITER));
            System.out.println("******insert into movie_genre table******");
            System.out.println(fill_movie_genre);
            executeScript(fill_movie_genre.split(QUERY_DELIMITER));
        } catch (IOException x) {
            System.err.println(x);
        }
    }

    public static void parseReview() {

        System.out.println("******insert into review table******");

        Path file = Paths.get("src\\main\\resources\\source\\review.txt");
        String fill_review = "";
        try (InputStream in = Files.newInputStream(file);
             BufferedReader reader =
                     new BufferedReader(new InputStreamReader(in))) {
            String line = null;
            int i = 0;
            String[] list = new String[3];
            while ((line = reader.readLine()) != null) {
                if (line.length() > 0) {
                    list[i++] = line;
                    if (i > list.length - 1) {
                        String movie_name_ru = list[0];
                        String first_name = list[1].split(BLANK_DELIMITER)[0];
                        String last_name = list[1].split(BLANK_DELIMITER)[1];
                        String comment = list[2];
                        fill_review += "insert into review (movie_id, user_id, comment) VALUES ((select movie_id from movie where movie_name_ru = '" + movie_name_ru + "'),"
                                + "(select user_id from users where first_name = '" + first_name + "' and last_name = '" + last_name + "'),'" + comment + "');\n";
                        i = 0;
                    }
                }
            }
        } catch (IOException x) {
            System.err.println(x);
        }
        System.out.println(fill_review);
        executeScript(fill_review.split(QUERY_DELIMITER));
    }
}